#-------------------------------------------------
#
# Project created by QtCreator 2014-07-19T20:46:30
#
#-------------------------------------------------

QT       += core gui

include(../buscloud/qextserialport/src/qextserialport.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = miFareStation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        rfidbasicpoller.cpp

HEADERS  += mainwindow.h\
            rfidbasicpoller.h

FORMS    += mainwindow.ui

RESOURCES += \
    qtresourceexample.qrc
