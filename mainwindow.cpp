#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qextserialenumerator.h"
#include "QSettings"
#include <unistd.h>
#include <QTimer>

char nfcData[16] = {0};

bool uiInitialized = false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    rfidPoller(NULL)
{

    ui->setupUi(this);
    ui->frame->hide();
    ui->frame_2->hide();
    ui->progressBar->hide();
    ui->dateEdit->setDate( QDate::currentDate() );
    ui->dateEdit_2->setDate( QDate::currentDate() );

    QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();

    foreach (QextPortInfo info, ports) {
//        qDebug() << "port name:"       << info.portName;
//        qDebug() << "friendly name:"   << info.friendName;
//        qDebug() << "physical name:"   << info.physName;
//        qDebug() << "enumerator name:" << info.enumName;
//        qDebug() << "vendor ID:"       << info.vendorID;
//        qDebug() << "product ID:"      << info.productID;

//        qDebug() << "===================================";
        if (info.portName.contains("COM"))
            ui->comboBox->addItem(info.portName, info.physName);
    }


    requestRead = false;
    uiInitialized = true;

    readSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    if(!uiInitialized)
        return;

    qDebug()<<ui->comboBox->itemData(index).toString();

    if (rfidPoller) {
        delete (rfidPoller);
    }

    rfidPoller = new RfidBasicPoller(ui->comboBox->itemText(index));

    //rfidPoller = new RfidBasicPoller(QLatin1String("COM1"));
    if(rfidPoller) {
        QObject::connect(rfidPoller, SIGNAL(valueChanged(QByteArray&)), this, SLOT(on_rfidRead(QByteArray&)));
    }

    writeSettings();
}

void MainWindow::on_rfidRead(QByteArray &rfidString)
{
    //this->ui->lineEdit->setText(QString(rfidString.c_str()));
    //qDebug()<<rfidString;
    if(requestRead) {
        QStringList mifareBytes = QString(rfidString).split("\n", QString::SkipEmptyParts);
        if(mifareBytes.size()!=16) {
            ui->statusBar->showMessage("Eroare la citirea cardului. Incercati din nou!");
        }

        foreach(QString str, mifareBytes) {
            if(str.startsWith("BYTE")) {
                int index, value;
                QStringList temp = str.split(" ");
                if(temp.size()==3) {
                    index = temp[1].toInt();
                    value = temp[2].toInt();
                    //qDebug()<<index<<" "<<value;
                    nfcData[index] = value;
                }
            }
        }
        if((nfcData[0] == 0) || (nfcData[0] == 1)) {
            ui->statusBar->clearMessage();
            ui->comboBox_2->setCurrentIndex(nfcData[0]);
            ui->lineEdit_5->setText(ui->comboBox_2->currentText());

            QDate date(2000+nfcData[3], nfcData[2], nfcData[1]);
            ui->dateEdit->setDate(date);
            ui->dateEdit_2->setDate(date);
            QDate lastValidationDate(2000+nfcData[8], nfcData[7], nfcData[6]);
            QTime lastValidationTime(nfcData[4], nfcData[5]);
            ui->dateTimeEdit->setDate(lastValidationDate);
            ui->dateTimeEdit->setTime(lastValidationTime);

            ui->lineEdit->setText(QString::number(nfcData[9]));
            ui->lineEdit_2->setText(QString::number(nfcData[11]));
            ui->spinBox->setValue(nfcData[13]);
            ui->spinBox_2->setValue(nfcData[13]);

        } else {
            ui->statusBar->showMessage("Card invalid");
        }
        requestRead = false;
    }


}

void MainWindow::updateProgress50_and_DelRfid()
{
    ui->progressBar->setValue(50);
    delete (rfidPoller);
}

void MainWindow::updateProgress100_and_NewRfid(){
    ui->progressBar->setValue(100);

    qDebug()<<ui->comboBox->currentText();
    rfidPoller = new RfidBasicPoller(ui->comboBox->currentText());

    //rfidPoller = new RfidBasicPoller(QLatin1String("COM1"));
    if(rfidPoller) {
        QObject::connect(rfidPoller, SIGNAL(valueChanged(QByteArray&)), this, SLOT(on_rfidRead(QByteArray&)));
    }

    ui->comboBox->setEnabled(true);
    ui->comboBox_2->setEnabled(true);
    ui->pushButton->show();
    ui->pushButton_2->show();
    ui->dateEdit->setEnabled(true);
    ui->spinBox->setEnabled(true);

    ui->progressBar->setValue(100);
    ui->progressBar->hide();
    ui->progressBar->setValue(0);

}

void MainWindow::on_pushButton_clicked()
{

    ui->comboBox->setDisabled(true);
    ui->comboBox_2->setDisabled(true);
    ui->pushButton->hide();
    ui->pushButton_2->hide();
    ui->dateEdit->setDisabled(true);
    ui->spinBox->setDisabled(true);

    ui->progressBar->show();
    char readCmd[6] = "READ\n";
    requestRead = true;
    rfidPoller->WriteData(readCmd, sizeof(readCmd));
    rfidPoller->flush();

    QTimer::singleShot(1500, this, SLOT(updateProgress50_and_DelRfid()));

    QTimer::singleShot(3000, this, SLOT(updateProgress100_and_NewRfid()));

}

void MainWindow::on_pushButton_2_clicked()
{
    ui->comboBox->setDisabled(true);
    ui->comboBox_2->setDisabled(true);
    ui->pushButton->hide();
    ui->pushButton_2->hide();
    ui->dateEdit->setDisabled(true);
    ui->spinBox->setDisabled(true);

    ui->progressBar->show();
    // Actualizeaza nfcData cu informatiile din GUI
    nfcData[13] = ui->spinBox->value();
    QDate expDate = ui->dateEdit->date();
    nfcData[1] = expDate.day();
    nfcData[2] = expDate.month();
    nfcData[3] = expDate.year()-2000;

    // Transmite pe seriala datele
    char writeCmd[16] = {0};
    for(int i=0; i<16; i++) {
        memset(writeCmd, 0, sizeof(writeCmd));
        sprintf(writeCmd, "STORE %d %d\n", i, nfcData[i]);
        rfidPoller->WriteData(writeCmd, strlen(writeCmd));
        usleep(10000);
    }
    sprintf(writeCmd, "WRITE\n");
    rfidPoller->WriteData(writeCmd, strlen(writeCmd));
    usleep(10000);

    QTimer::singleShot(1500, this, SLOT(updateProgress50_and_DelRfid()));

    QTimer::singleShot(3000, this, SLOT(updateProgress100_and_NewRfid()));

}

void MainWindow::on_comboBox_2_currentIndexChanged(int index)
{
    nfcData[0] = index;
    qDebug()<<"Schimbare tip="<<index<<ui->comboBox_2->currentText();
    if (index==0) {
        ui->frame->hide();
        ui->frame_2->hide();
        ui->pushButton_2->setEnabled(true);
    }
    else {
        ui->frame->show();
        ui->frame_2->show();
        ui->pushButton_2->setDisabled(true);
    }
    ui->lineEdit_5->setText(ui->comboBox_2->currentText());
}

void MainWindow::readSettings()
{
    qDebug()<<__FUNCTION__;
    QSettings settings("OpenDev Labs", "BusCloud RFID Manager");
    settings.beginGroup("serialPort");
    qDebug()<<settings.value("namePort");
    ui->comboBox->insertItem(0, settings.value("namePort", NAME_SERIAL_PORT).toString(), settings.value("devPort", DEV_SERIAL_PORT).toString());
    ui->comboBox->setCurrentIndex(0);
    settings.endGroup();
}

void MainWindow::writeSettings()
{
    qDebug()<<__FUNCTION__;
    QSettings settings("OpenDev Labs", "BusCloud RFID Manager");
    settings.beginGroup("serialPort");
    settings.setValue("namePort", ui->comboBox->itemText(ui->comboBox->currentIndex()));
    settings.setValue("devPort", ui->comboBox->itemData(ui->comboBox->currentIndex()));
    qDebug()<<settings.value("namePort");
    settings.endGroup();
}

void MainWindow::on_MainWindow_destroyed()
{
    qDebug()<<__FUNCTION__;
}
