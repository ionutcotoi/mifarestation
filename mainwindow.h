#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "rfidbasicpoller.h"

#ifdef _WIN32
#define DEV_SERIAL_PORT  "COM26"
#define NAME_SERIAL_PORT "COM26"
#else
#define DEV_SERIAL_PORT  "/dev/ttyUSB0"
#define NAME_SERIAL_PORT "ttyUSB0"
#endif

namespace Ui {
class MainWindow;
}

enum last_operation {
    READ, WRITE
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_comboBox_currentIndexChanged(int index);
    void on_rfidRead(QByteArray&);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_comboBox_2_currentIndexChanged(int index);

    void on_MainWindow_destroyed();

    void updateProgress50_and_DelRfid();
    void updateProgress100_and_NewRfid();

private:
    Ui::MainWindow *ui;
    RfidBasicPoller *rfidPoller;
    bool requestRead;
    void readSettings();
    void writeSettings();

    int timerId;

protected:

};

#endif // MAINWINDOW_H
