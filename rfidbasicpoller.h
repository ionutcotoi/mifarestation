#ifndef RFIDBASICPOLLER_H
#define RFIDBASICPOLLER_H


#include <QObject>
#include <qextserialport.h>
#include <QDebug>
#include <iostream>

class RfidBasicPoller : public QObject
{
    Q_OBJECT
private:
    QextSerialPort *port;
    QByteArray bytes;
    bool hasStartCharacter;

public:
    explicit RfidBasicPoller (const QString & portName, QObject *parent = 0);
    ~RfidBasicPoller()
        {port->close(); delete (port);}
    qint64 WriteData(const char * data, qint64 maxSize);
    void flush() {
        port->flush();
    }


protected:
public slots:
    virtual void onReadyRead();
    void onDsrChanged(bool status);

signals:
    void valueChanged(QByteArray&);

};



#endif // RFIDBASICPOLLER_H
