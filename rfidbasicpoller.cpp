#include "rfidbasicpoller.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef __arm
#include <sys/ioctl.h>
#endif
#include <fcntl.h>
#include <QVariant>
#include <QDateTime>

struct ReadData {
    ReadData(){memset(data, 0, sizeof(data));}

    char data[50];
    size_t sz;

};

#ifdef __arm__

//TODO Icotoi - move this code out of here
#define PWM_IOCTL_SET_FREQ		1
#define PWM_IOCTL_STOP			0

static int fd = -1;
static void close_buzzer(void);
static void open_buzzer(void)
{
    fd = open("/dev/pwm", 0);
    if (fd < 0) {
        perror("open pwm_buzzer device");
     //   exit(1);
    }

    // any function exit call will stop the buzzer
    // atexit(close_buzzer);
}

static void close_buzzer(void)
{
    if (fd >= 0) {
        ioctl(fd, PWM_IOCTL_STOP);
        close(fd);
        fd = -1;
    }
}

static void set_buzzer_freq(int freq)
{
    // this IOCTL command is the key to set frequency
    int ret = ioctl(fd, PWM_IOCTL_SET_FREQ, freq);
    if(ret < 0) {
        perror("set the frequency of the buzzer");
      //  exit(1);
    }
}
static void stop_buzzer(void)
{
    int ret = ioctl(fd, PWM_IOCTL_STOP);
    if(ret < 0) {
        perror("stop the buzzer");
    //    exit(1);
    }
}

//---
#endif


RfidBasicPoller::RfidBasicPoller(const QString & portName, QObject *parent) :
    QObject(parent)
{

     qDebug() << __FUNCTION__;
     this->port = new QextSerialPort(portName, QextSerialPort::EventDriven);
     port->setBaudRate(BAUD115200);
     port->setFlowControl(FLOW_OFF);
     port->setParity(PAR_NONE);
     port->setDataBits(DATA_8);
     port->setStopBits(STOP_1);

     if (port->open(QIODevice::ReadWrite) == true) {
         connect(port, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
         connect(port, SIGNAL(dsrChanged(bool)), this, SLOT(onDsrChanged(bool)));
         if (!(port->lineStatus() & LS_DSR))
             qDebug() << "warning: device is not turned on";
         qDebug() << "listening for data on" << port->portName();
     }
     else {
         qDebug() << "device failed to open:" << port->errorString();
     }
 }

void RfidBasicPoller::onReadyRead()
{
    QByteArray theseBytes;

    int numBytes = port->bytesAvailable();

    theseBytes.resize(numBytes);
    theseBytes = port->read(numBytes);

    if(theseBytes.size() > 0) {
        qDebug()<<"RFID: "<<theseBytes;
        emit(valueChanged(theseBytes));
    }
}

void RfidBasicPoller::onDsrChanged(bool status)
{
    if (status)
        qDebug() << "device was turned on";
    else
        qDebug() << "device was turned off";
}

qint64 RfidBasicPoller::WriteData(const char * data, qint64 maxSize)
{
    qDebug()<<data;
    return port->writeData(data, maxSize);
}

